CREATE DATABASE  IF NOT EXISTS `ygly` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `ygly`;
-- MySQL dump 10.13  Distrib 5.6.11, for Win32 (x86)
--
-- Host: localhost    Database: ygly
-- ------------------------------------------------------
-- Server version	5.6.21

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `label`
--

DROP TABLE IF EXISTS `label`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `label` (
  `Stu_Id` int(8) NOT NULL DEFAULT '0',
  PRIMARY KEY (`Stu_Id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `label`
--

LOCK TABLES `label` WRITE;
/*!40000 ALTER TABLE `label` DISABLE KEYS */;
/*!40000 ALTER TABLE `label` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pro`
--

DROP TABLE IF EXISTS `pro`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pro` (
  `Id` int(8) NOT NULL AUTO_INCREMENT,
  `Name` varchar(20) DEFAULT NULL,
  `Com_Id` int(8) DEFAULT NULL,
  `Stu_Sum` int(20) DEFAULT NULL,
  `State` int(20) DEFAULT NULL,
  `Content` varchar(100) DEFAULT NULL,
  `Tel` varchar(20) DEFAULT NULL,
  `Createtime` datetime DEFAULT NULL,
  `Delete` int(6) DEFAULT '0',
  `Com_Name` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pro`
--

LOCK TABLES `pro` WRITE;
/*!40000 ALTER TABLE `pro` DISABLE KEYS */;
INSERT INTO `pro` VALUES (1,'项目1',NULL,NULL,NULL,'通过自定义标签实现highcharts 3D图表展示，借鉴了Android适配器的思想，通过固定的数据源，展现相关图表，无需考虑图表内...',NULL,NULL,0,'企业1'),(2,'项目2',NULL,NULL,NULL,'通过自定义标签实现highcharts 3D图表展示，借鉴了Android适配器的思想，通过固定的数据源，展现相关图表，无需考虑图表内...',NULL,NULL,0,'企业2');
/*!40000 ALTER TABLE `pro` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pro_label`
--

DROP TABLE IF EXISTS `pro_label`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pro_label` (
  `Pro_Id` int(8) NOT NULL DEFAULT '0',
  PRIMARY KEY (`Pro_Id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pro_label`
--

LOCK TABLES `pro_label` WRITE;
/*!40000 ALTER TABLE `pro_label` DISABLE KEYS */;
/*!40000 ALTER TABLE `pro_label` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `stu_pro`
--

DROP TABLE IF EXISTS `stu_pro`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `stu_pro` (
  `Id` int(8) NOT NULL AUTO_INCREMENT,
  `Stu_Id` int(8) DEFAULT NULL,
  `Stu_Name` varchar(20) DEFAULT NULL,
  `Pro_Id` int(8) DEFAULT NULL,
  `Pro_Name` varchar(20) DEFAULT NULL,
  `Com_Id` int(8) DEFAULT NULL,
  `Com_Name` varchar(20) DEFAULT NULL,
  `State` int(20) DEFAULT NULL,
  `Createtime` datetime DEFAULT NULL,
  `Grade` int(6) DEFAULT '0',
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `stu_pro`
--

LOCK TABLES `stu_pro` WRITE;
/*!40000 ALTER TABLE `stu_pro` DISABLE KEYS */;
/*!40000 ALTER TABLE `stu_pro` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user` (
  `Id` int(8) NOT NULL AUTO_INCREMENT,
  `User` varchar(20) NOT NULL,
  `Pass` varchar(20) NOT NULL,
  `Name` varchar(20) DEFAULT NULL,
  `Sex` varchar(20) DEFAULT NULL,
  `Tel` varchar(20) DEFAULT NULL,
  `Loc` varchar(20) DEFAULT NULL,
  `Nick` varchar(20) DEFAULT NULL,
  `Grade` int(6) DEFAULT NULL,
  `Type` int(6) DEFAULT '0',
  `Delete` int(6) DEFAULT '0',
  `Motto` varchar(40) DEFAULT NULL,
  `Introduction` varchar(20) DEFAULT NULL,
  `Province` varchar(20) DEFAULT NULL,
  `City` varchar(20) DEFAULT NULL,
  `Email` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` VALUES (1,'1312190108','123456','张伦亮',NULL,'110','浙江工商大学','Lawliet',NULL,0,0,'这同学很懒，什么都没写','长路漫漫，唯基作伴','湖南','江山','779623037@qq.com');
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-04-20 13:57:34
