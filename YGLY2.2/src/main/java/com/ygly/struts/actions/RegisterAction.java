package com.ygly.struts.actions;

import com.opensymphony.xwork2.ActionSupport;
import com.ygly.struts.bizs.UserBiz;
import com.ygly.struts.models.User;

public class RegisterAction extends ActionSupport {

	/**
	 * @return
	 */
	private User user;
	public User getUser() {
		return user;
	}
	public void setUser(User user) {
		this.user = user;
	}
	public String execute() {
		UserBiz userbiz = new UserBiz();
		ContextAction context = new ContextAction();
		int result = 0;
		try {
			result = userbiz.Register(user);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		if(result == 1){
			context.request("error", "该用户已存在");
			return "FALSE";
		}else if(result == 2){
			context.request("error", "密码输入不一致");
			return "FALSE";
		}else{
			return "SUCCESS";
		}
	}
}