package com.ygly.struts.actions;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class SqlAction {
	private Statement stmt = null;
	private Connection conn = null;
	ResultSet rs = null;
	public SqlAction(){}
	public void OpenConn()throws Exception
	{
		try
		{
			 Class.forName("com.mysql.jdbc.Driver");
			 String url = "jdbc:mysql://localhost:3306/ygly?"  + "user=root&password=123456&useUnicode=true&characterEncoding=UTF8";
			 conn = DriverManager.getConnection(url);
		}
		catch(SQLException e)
		{
			System.err.println("Data.executeQuery:"+e.getMessage());
		}
	}
	public ResultSet executeQuery(String sql)
	{
		rs = null;
		try
		{
			stmt = conn.createStatement();
			rs = stmt.executeQuery(sql);
		}
		catch(SQLException e)
		{
			System.err.println("Data.executeQuery:"+e.getMessage());
		}
		return rs;
	}
	public void executeUpdata(String sql)
	{
		stmt = null;
		try
		{
			stmt = conn.createStatement();
			stmt.executeUpdate(sql);
		}
		catch(SQLException e)
		{
			System.err.println("Data.executeUpdate:"+e.getMessage());
		}
	}
	public void closeStmt()
	{
		try
		{
			stmt.close();
		}
		catch(SQLException e)
		{
			System.err.println("Date.executeQuery:"+e.getMessage());
		}
	}
	public void closeConn()
	{
		try
		{
			conn.close();
		}
		catch(SQLException e)
		{
			System.err.println("Date.executeQuery:"+e.getMessage());
		}
	}
}
