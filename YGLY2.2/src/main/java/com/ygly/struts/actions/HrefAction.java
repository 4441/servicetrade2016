package com.ygly.struts.actions;

import java.util.Map;

import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;

public class HrefAction extends ActionSupport {

	/**
	 * @return
	 */
	public String execute() {
		// TODO Auto-generated method stub
		ActionContext ctx=ActionContext.getContext();  
		Map session=ctx.getSession();  
		String login = (String) session.get("login");
		if(login!=null&&login.equals("true")){
			return "PAGE2";
		}else {
			return "PAGE1";
		}
	}
}