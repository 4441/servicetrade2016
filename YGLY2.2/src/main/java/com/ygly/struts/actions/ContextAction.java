package com.ygly.struts.actions;

import java.util.Map;

import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;

public class ContextAction extends ActionSupport{
	public ActionContext actionContext = ActionContext.getContext();
	public void request(String key, Object value){
		Map<String,Object> request = (Map) actionContext.get("request"); 
		request.put(key, value); 
	}
	public void session(String key, Object value){
		Map<String,Object> session = actionContext.getSession();
		session.put(key, value);
	}
	public void appliction(String key, Object value){
		Map<String,Object> application  = actionContext.getApplication(); 
		application.put(key, value);
	}
}
