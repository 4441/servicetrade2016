package com.ygly.struts.actions;

import com.opensymphony.xwork2.ActionSupport;

public class LogoutAction extends ActionSupport {

	/**
	 * @return
	 */
	public String execute() {
		// TODO Auto-generated method stub
		ContextAction context = new ContextAction();
		context.session("login", "false");
		return "SUCCESS";
	}
}