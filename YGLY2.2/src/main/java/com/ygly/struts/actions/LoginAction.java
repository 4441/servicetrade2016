package com.ygly.struts.actions;

import java.sql.*;
import java.util.Map;

import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;
import com.ygly.struts.bizs.UserBiz;
import com.ygly.struts.models.User;

public class LoginAction extends ActionSupport {

	/**
	 * @return
	 */
	private User user;
	public User getUser() {
		return user;
	}
	public void setUser(User user) {
		this.user = user;
	}
	public String execute() {
		// TODO Auto-generated method stub
		ActionContext ctx=ActionContext.getContext();  
		Map session=ctx.getSession();  
		String login = (String) session.get("login");
		if(login!=null&&login.equals("true")){
			return "STUDENT";
		}
		UserBiz userbiz = new UserBiz();
		ContextAction context = new ContextAction();
		int result = 2;
		try {
			result = userbiz.isExists(user);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		if(result==0){
			try {
				setUser(userbiz.queryuser(user.getUser()));
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			context.session("user", user);
			context.session("login", "true");
			return "STUDENT";
		}else if(result==1){
			try {
				setUser(userbiz.queryuser(user.getUser()));
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			context.session("user", user);
			context.session("login", "true");
			return "COMPANY";
		}
		else{
			context.request("error","*");
			return "FALSE";
		}
	}
}