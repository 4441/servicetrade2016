package com.ygly.struts.models;

import java.util.Date;

public class Pro {
	private String name;
	private int com_id;
	private String com_name;
	private int[] open;
	private int stu_sum;
	private int state;
	private String intr;
	private String content;
	private String tel;
	private Date createtime;
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getCom_id() {
		return com_id;
	}
	public void setCom_id(int com_id) {
		this.com_id = com_id;
	}
	public String getCom_name() {
		return com_name;
	}
	public void setCom_name(String com_name) {
		this.com_name = com_name;
	}
	public int[] getOpen() {
		return open;
	}
	public void setOpen(int[] open) {
		this.open = open;
	}
	public int getStu_sum() {
		return stu_sum;
	}
	public void setStu_sum(int stu_sum) {
		this.stu_sum = stu_sum;
	}
	public int getState() {
		return state;
	}
	public void setState(int state) {
		this.state = state;
	}
	public String getIntr() {
		return intr;
	}
	public void setIntr(String intr) {
		this.intr = intr;
	}
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	public String getTel() {
		return tel;
	}
	public void setTel(String tel) {
		this.tel = tel;
	}
	public Date getCreatetime() {
		return createtime;
	}
	public void setCreatetime(Date createtime) {
		this.createtime = createtime;
	}
}
