package com.ygly.struts.href;

import com.ygly.struts.models.*;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import com.opensymphony.xwork2.ActionSupport;
import com.ygly.struts.actions.SqlAction;

public class Href2Action extends ActionSupport {

	/**
	 * @return
	 */
	public List<Pro> prolist;
	public List<Pro> list = new ArrayList<Pro>();
	public List<Pro> getProlist() {
		return prolist;
	}
	public void setProlist(List<Pro> prolist) {
		this.prolist = prolist;
	}
	public String execute() throws Exception{
		// TODO Auto-generated method stub
		String query = "select * from pro";
		SqlAction sql = new SqlAction();
		sql.OpenConn();
		ResultSet rs = sql.executeQuery(query);
		while(rs.next()){
			Pro pro = new Pro();
			pro.setName(rs.getString("Name"));
			pro.setCom_id(rs.getInt("Com_Id"));
			pro.setCom_name(rs.getString("Com_Name"));
			pro.setStu_sum(rs.getInt("Stu_Sum"));
			pro.setState(rs.getInt("State"));
			pro.setIntr(rs.getString("Introduction"));
			pro.setContent(rs.getString("Content"));
			pro.setTel(rs.getString("tel"));
			pro.setCreatetime(rs.getDate("Createtime"));
			list.add(pro);
		}
		this.setProlist(list);
		return "SUCCESS";
	}
}