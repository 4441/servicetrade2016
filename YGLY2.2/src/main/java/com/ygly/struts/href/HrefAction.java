package com.ygly.struts.href;

import java.util.Map;

import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;
import com.ygly.struts.models.User;

public class HrefAction extends ActionSupport {

	/**
	 * @return
	 */
	public String execute() {
		// TODO Auto-generated method stub
		ActionContext ctx=ActionContext.getContext();  
		Map session=ctx.getSession();  
		String login = (String) session.get("login");
		User user = (User ) session.get("user");
		if(user!=null&&login!=null&&login.equals("true")){
			if(user.getType()==0){
				return "PAGE2";
			}else{
				return "PAGE3";
			}
		}else {
			return "PAGE1";
		}
	}
}