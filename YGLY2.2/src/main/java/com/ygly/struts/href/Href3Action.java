package com.ygly.struts.href;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import com.opensymphony.xwork2.ActionSupport;
import com.ygly.struts.actions.SqlAction;
import com.ygly.struts.models.User;

public class Href3Action extends ActionSupport {

	/**
	 * @return
	 */
	public List<User> enterlist;
	public List<User> list = new ArrayList<User>();
	public List<User> getEnterlist() {
		return enterlist;
	}
	public void setEnterlist(List<User> enterlist) {
		this.enterlist = enterlist;
	}
	public String execute() throws Exception {
		// TODO Auto-generated method stub
		String query = "Select * from user where type = 1";
		SqlAction sql = new SqlAction();
		sql.OpenConn();
		ResultSet rs = sql.executeQuery(query);
		while(rs.next()){
			User user = new User();
			user.setUser(rs.getString("User"));
			user.setName(rs.getString("Name"));
			user.setIntroduction(rs.getString("Introduction"));
			user.setProsum(rs.getInt("Prosum"));
			if(rs.getString("User").equals("admin"))	continue;
			list.add(user);
		}
		this.setEnterlist(list);
		return "SUCCESS";
	}
}