package com.ygly.struts.pro;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;

import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;
import com.ygly.struts.actions.SqlAction;
import com.ygly.struts.models.Pro;
import com.ygly.struts.models.User;

public class CreatePro extends ActionSupport {

	/**
	 * @return
	 */
	private Pro pro; 
	public Pro getPro() {
		return pro;
	}
	public void setPro(Pro pro) {
		this.pro = pro;
	}
	public String execute() {
		// TODO Auto-generated method stub
		ActionContext ctx=ActionContext.getContext();  
		Map session=ctx.getSession();  
		User user = (User ) session.get("user");
		String name = pro.getName();
		int com_id = user.getId();
		String com_name = user.getName();
		int open;
		if(pro.getOpen() == null)	open = 0;
		else open = 1;
		String intr = pro.getIntr();
		String content = pro.getContent();
		Date dt = new Date();
		SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		String createtime = df.format(dt);
		String insert = "INSERT INTO `ygly`.`pro` (`Name`, `Com_Id`, `Com_Name`, `Open`, `Introduction`, `Content`, `Createtime`) VALUES ('"+name+"', '"+com_id+"', '"+com_name+"', '"+open+"', '"+intr+"', '"+content+"', '"+createtime+"')";
		SqlAction sql = new SqlAction();
		try {
			sql.OpenConn();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		sql.executeUpdata(insert);
		return "SUCCESS";
	}
}