package com.ygly.struts.bizs;

import com.ygly.struts.models.User;
import java.sql.*;

import com.ygly.struts.actions.SqlAction;

public class UserBiz {
	public int isExists(User user) throws Exception{
		SqlAction sql = new SqlAction();
		ResultSet rs = null;
		String query = "select * from user where User = '"+user.getUser()+"' and Pass = '"+user.getPwd()+"'";
		sql.OpenConn();
		rs = sql.executeQuery(query);
		if(rs.next()){
			if(rs.getInt("Type")==0){
				return 0;
			}else {
				return 1;
			}
		}else {
			return 2;
		}
	}
	public int Register(User user) throws Exception {
		if(!user.getPwd().equals(user.getPwd2())){
			return 2;
		}
		SqlAction sql = new SqlAction();
		ResultSet rs = null;
		String query = "select * from user where User = '"+user.getUser()+"' and Pass = '"+user.getPwd()+"'";
		sql.OpenConn();
		rs = sql.executeQuery(query);
		if(rs.next()){
			return 1;
		}
		query = "INSERT INTO `ygly`.`user` (`User`, `Pass`) VALUES ('"+user.getUser()+"', '"+user.getPwd()+"')";
		sql.executeUpdata(query);
		return 0;
	}
	public User queryuser(String name) throws Exception{
		User user = new User();
		SqlAction sql = new SqlAction();
		ResultSet rs = null;
		String query = "select * from user where User = '"+name+"'";
		sql.OpenConn();
		rs = sql.executeQuery(query);
		if(rs.next()){
			user.setId(rs.getInt("Id"));
			user.setUser(rs.getString("User"));
			user.setPwd(rs.getString("Pass"));
			user.setGrade(rs.getInt("Grade"));
			user.setLoc(rs.getString("Loc"));
			user.setName(rs.getString("Name"));
			user.setNick(rs.getString("Nick"));
			user.setSex(rs.getString("Sex"));
			user.setTel(rs.getString("Tel"));
			user.setType(rs.getInt("Type"));
			user.setMotto(rs.getString("Motto"));
			user.setIntroduction(rs.getString("Introduction"));
			user.setProvince(rs.getString("Province"));
			user.setCity(rs.getString("City"));
			user.setEmail(rs.getString("Email"));
		}
		return user;
	}
}
