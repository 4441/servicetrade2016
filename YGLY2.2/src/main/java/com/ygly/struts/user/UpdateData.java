package com.ygly.struts.user;

import java.util.Map;

import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;
import com.ygly.struts.actions.ContextAction;
import com.ygly.struts.actions.SqlAction;
import com.ygly.struts.models.User;

public class UpdateData extends ActionSupport {

	/**
	 * @return
	 */
	private User user;
	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}
	public String execute() {
		// TODO Auto-generated method stub
		ContextAction context = new ContextAction();
		ActionContext ctx = ActionContext.getContext();
		Map session = ctx.getSession();
		User user2 = (User )session.get("user");
		SqlAction sql = new SqlAction();
		try {
			sql.OpenConn();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		String update = "UPDATE `ygly`.`user` SET `Name`='"+user.getName()+"', `Tel`='"+user.getTel()+"', " +
				"`Loc`='"+user.getLoc()+"', `Nick`='"+user.getNick()+"', `Motto`='"+user.getMotto()+"', " +
				"`Introduction`='"+user.getIntroduction()+"', `Province`='"+user.getProvince()+"', " +
						"`City`='"+user.getCity()+"', `Email`='"+user.getEmail()+"' WHERE `User`='"+user2.getUser()+"'";
		sql.executeUpdata(update);
		return "SUCCESS";
	}
}