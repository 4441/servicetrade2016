<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <base href="<%=basePath%>">
	<link rel="stylesheet" href="css/personaldata.css">
  </head>
  
  <body>
  	<jsp:include page="center.jsp"></jsp:include>
  	<div class="content">
  		<h3>我的信息</h3>
  		
  		<form action="UpdateData" method="post">
  		<label>昵称</label><br>
  		<div class="text-box1">
  			<input type="text" name="user.nick" placeholder="昵称" required="required" class="item-row" value="${user.nick}" />
  		</div><br>
  		
  		<label>个性签名</label><br>
  		<div class="text-box1">
  			<input type="text" name="user.motto" placeholder="个性签名" required="required" class="item-row" value="${user.motto}"  />
  		</div><br>
  		
  		<label>个人介绍</label><br>
  		<div class="text-box1">
  			<textarea name="user.introduction" rows="7" class="textarea-box" placeholder="个人介绍" >${user.introduction}</textarea>
  		</div><br>
  		
  		<h3>更多信息</h3>
  		
  		<label>真实姓名</label>
  		<label class="label-distance">手机号码</label><br>
  		<div class="text-box2">
  			<input type="text" name="user.name" placeholder="姓名" required="required" class="item-row2" value="${user.name}" />&nbsp&nbsp&nbsp
  			<input type="text" name="user.tel" placeholder="手机号码" required="required" class="item-row2" value="${user.tel}"  />
  		</div><br>
  		
  		<label>现居省份</label>
  		<label class="label-distance">现居城市</label><br>
  		<div class="text-box2">
  			<input type="text" name="user.province" placeholder="省份" required="required" class="item-row2" value="${user.province}" />&nbsp&nbsp&nbsp
  			<input type="text" name="user.city" placeholder="城市" required="required" class="item-row2" value="${user.city}" />
  		</div><br>
  		
  		<label>电子邮箱</label>
  		<label class="label-distance">所在学校</label><br>
  		<div class="text-box2">
  			<input type="text" name="user.email" placeholder="电子邮箱" required="required" class="item-row2" value="${user.email}" />&nbsp&nbsp&nbsp
  			<input type="text" name="user.loc" placeholder="所在学校" required="required" class="item-row2" value="${user.loc}" />
  		</div><br>
  		<div class="item-row">
  				<button class="login-btn">更新</button>
  		</div>
  		</form>	
  	</div>
  </body>
</html>