<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <base href="<%=basePath%>">
	
	<link rel="stylesheet" href="css/student.css">
  </head>
  
  <body>
  	<jsp:include page="/public/head.jsp"></jsp:include>
  		<div class="menu">
  			<div class="menu-head">
  				<div class="picture">
    				<a href="student/picture.jsp"><img src="img/bg.jpg"></a>
				</div>
				<div class="information">
					${user.nick}
				</div>
  			</div>
  			<div class="item-list">
  				<div class="item">
  					<a href="student/dynamic.jsp" class="item-href">动态</a>
  				</div>
  				<div class="item">
  					<a href="student/message.jsp" class="item-href">私信</a>
  				</div>
  				<div class="item">
  					<a href="student/myproject.jsp" class="item-href">我的项目</a>
  				</div>
  				<div class="item">
  					<a href="student/personaldata.jsp" class="item-href">个人资料</a>
  				</div>
  				<div class="item">
  					<a href="student/accountdata.jsp" class="item-href">修改账户</a>
  				</div>
  			</div>
  			
  		</div>
  </body>
</html>