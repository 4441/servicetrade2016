<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <base href="<%=basePath%>">
	
	<link rel="stylesheet" href="css/accountdata.css">
  </head>
  
  <body>
  	<jsp:include page="center.jsp"></jsp:include>
  	<div class="content">
  	<form action="UpdatePwd" method="post">
  		<h3>修改账户</h3>
  		
  		<label>当前密码</label><br>
  		<div class="text-box1">
  			<input type="password" name="up.pwd1" placeholder="6-15位，包含数字和字母" required="required" class="item-row"  />
  		</div><br>
  		
  		<label>新密码</label><br>
  		<div class="text-box1">
  			<input type="password" name="up.pwd2" placeholder="新密码和当前密码不能相同" required="required" class="item-row"  />
  		</div><br>
  		
  		<label>确认密码</label><br>
  		<div class="text-box1">
  			<input type="password" name="up.pwd3" placeholder="确认密码和新密码保持一致" required="required" class="item-row"  />
  		</div><br>
  		
  		<div class="item-row">
  				<button class="login-btn">提交修改</button>	${re}
  		</div>
  	</form>
  	</div>
  </body>
</html>