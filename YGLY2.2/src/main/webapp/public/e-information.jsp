<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <base href="<%=basePath%>">
	
	<link rel="stylesheet" href="css/e-information.css">
	
  </head>
  
  <body>
  	<jsp:include page="head.jsp"></jsp:include>
  	<div class="content">
   		<div class="left">
   			<div class="userhead">
   				<div class="picture">
   					<img src="img/bg.jpg">
   				</div>
   				<div class="line">
   					用户名
   					<div class="line-right">
   						<button>私信</button>
   					</div>
   				</div>
   				<div class="line2">签名</div>
   			</div><!-- userhead -->
   			
   			<div class="dynamic">
   				<h2>他的动态</h2>
   				<div class="dynamic-left">
  						通过自定义标签实现highcharts 3D图表展示，借鉴了Android适配器的思想，通过固定的数据源，展现相关图表，无需考虑图表内...
  				</div>
  				<div class="dynamic-right">
  					time
  				</div>
  				
  				<%
				for(int i=0;i<5;i++){
				%>
   				<div class="dynamic-left">
  						通过自定义标签实现highcharts 3D图表展示，借鉴了Android适配器的思想，通过固定的数据源，展现相关图表，无需考虑图表内...
  				</div>
  				<div class="dynamic-right">
  					time
  				</div>
				<%
				}
				 %>
				 
   			</div>
  		</div>
  		<div class="right">
			<div class="project-list">
				<div class="list-head">
					项目
				</div>
				<%
				for(int i=0;i<5;i++){
				%>
				<div class="list-item">
					<a href="public/project.jsp" class="item-href">发布的项目</a>
				</div>
				<%
				}
				 %>
				 <div class="more">
					<a href="javascript:void()" class="item-href">查看更多</a>
				</div>
			</div>
  		</div>
  	</div>
  </body>
</html>