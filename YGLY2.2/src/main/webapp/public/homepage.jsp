<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <base href="<%=basePath%>">
    
	<link rel="stylesheet" href="css/public.css">
	<link rel="stylesheet" href="css/homepage.css">
  </head>
  
  <body>
  	<jsp:include page="head.jsp"></jsp:include>
  	<div class="content">
  		<div class="introduce">
  			<div class="introduce-name">
  				创新创业服务交易对接平台
  			</div>
  			<div class="introduce-intro">
  				学生与企业之间的交互平台
  			</div>
  		</div>
  	</div>
  </body>
</html>
