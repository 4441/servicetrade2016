<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <base href="<%=basePath%>">
	
	<link rel="stylesheet" href="css/register.css">
	
  </head>
  
  <body>
  	<jsp:include page="head.jsp"></jsp:include>
   	<div class="content">
  		<div class="item-head">
  			用户注册${error}
  		</div>
  		<form action="Register" method="post">
  		<div class="form-style">
  			<div class="item-row">
  				<input class="input-box" type="text" name="user.user" placeholder="set a user name"/>
  			</div>
  			<div class="item-row">
  				<input class="input-box" type="password" name="user.pwd" placeholder="set a password"/>
  			</div>
  			<div class="item-row">
  				<input class="input-box" type="password" name="user.pwd2" placeholder="confirm your password"/>
  			</div>
  			<div class="item-row">
  				<button class="register-btn">注册</button>
  			</div>
  			<div class="item-row" style="height:20px;">
  				<a href="public/login.jsp" class="register-href">已有账号</a>
  			</div>
  		</div>
  		</form>
  	</div>
  </body>
</html>
