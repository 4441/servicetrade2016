<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@taglib prefix="s" uri="/struts-tags" %>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <base href="<%=basePath%>">
    
	<link rel="stylesheet" href="css/public.css">
  </head>
  
  <body>
  	<jsp:include page="../head.jsp"></jsp:include>
  	<div class="content">
  		<div class="content-head">
  			<a href="Href2" class="content-href" id="recommend">推荐项目</a>
  			<a href="public/project-list/hot.jsp" class="content-href" id="hot">热门项目</a>
  			<a href="public/project-list/latest.jsp" class="content-href" id="latest">最近更新</a>
  		</div>
  		<div class="list"> 
			<s:iterator value="list">
  			<div class="list-item">
  				<div class="item-left">
  					<div class="left-up">
  						<a href="public/project.jsp?name=${name}" class="item-href"><s:property value="name"/>/ <s:property value="com_name"/></a>
  					</div>
  					<div class="left-down">
  						<s:property value="intr"/>
  					</div>
  				</div>
  				<div class="item-right">
  					<div>
  						<a href="javascript:void()" class="item-href2">收藏0</a>
  						<a href="javascript:void()" class="item-href2">评论0</a>
  						<a href="javascript:void()" class="item-href2">参与0</a>
  					</div>
  				</div>
  			</div>
  			</s:iterator><!-- list-item -->
 	 	</div><!-- list -->
  	</div>
  </body>
</html>