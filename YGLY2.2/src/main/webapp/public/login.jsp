<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <base href="<%=basePath%>">
    
	<link rel="stylesheet" href="css/login.css">
	
  </head>
  
  <body>
  	<jsp:include page="head.jsp"></jsp:include>
   	<div class="content">
  		<div class="item-head">
  			用户登陆
  		</div>
  		<form action="Login" method="post">
  		<div class="form-style">
  			<div class="item-row">
  				<input class="input-box" type="text" name="user.user" placeholder="user"/>
  			</div>
  			<div class="item-row">
  				<input class="input-box" type="password" name="user.pwd" placeholder="password"/>
  			</div>
  			<div class="item-row" style="height:30px;">
				<input class="" type="checkbox"/>记住我
				<a href="javascript" class="login-href">忘记密码？</a>
  			</div>
  			<div class="item-row">
  				<button class="login-btn">登陆</button>
  			</div>
  			<div class="item-row" style="height:20px;">
  				<a href="public/register.jsp" class="login-href">注册新账号</a>
  			</div>
  		</div>
  		</form>
  	</div>
  </body>
</html>
