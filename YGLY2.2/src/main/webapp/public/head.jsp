<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@taglib prefix="s" uri="/struts-tags" %>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <base href="<%=basePath%>">
    
    <title>创新创业服务交易对接平台</title>
	
	<link rel="stylesheet" href="css/head.css">
  </head>
  
  <body>
	<div class="nav">	<!-- nav：抬头样式 -->
		<div class="nav-content">
			<div class="nav-content-item">
				<a href="Href1" class="nav-href">创新创业服务交易对接平台</a>
				<a href="Href2" class="nav-href">项目列表</a>
				<a href="Href3" class="nav-href">企业列表</a>
				<a href="javascript:void()" class="nav-href">关于平台</a>
			</div>
			<div class="nav-content-item-right">
			<s:if test="#session.login==null||#session.login=='false'">
				<a href="public/register.jsp" class="nav-href">注册</a>
				<a href="public/login.jsp" class="nav-href">登陆</a>
			</s:if><s:else>
  			  	<a href="Logout" class="nav-href">注销</a>
  			 </s:else>
			</div>
		</div>
	</div>
  </body>
</html>
