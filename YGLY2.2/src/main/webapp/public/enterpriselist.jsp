<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@taglib prefix="s" uri="/struts-tags" %>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <base href="<%=basePath%>">
	
	<link rel="stylesheet" href="css/public.css">
  </head>
  
  <body>
  	<jsp:include page="head.jsp"></jsp:include>
	<div class="content">
		<div class="content-head">
  			
  		</div>
		<div class="list">
			<s:iterator value="list">
			<div class="list-item">
	  				<div class="item-left">
	  					<div class="left-up">
	  						<a href="public/e-information.jsp" class="item-href"> <s:property value="name"/> </a>
	  					</div>
	  					<div class="left-down">
	  						<s:property value="introduction"/>
	  					</div>
	  				</div>
	  				<div class="item-right">
  					<div>
  						<label class="item-href2">项目：<s:property value="prosum"/> </label>
  					</div>
  				</div>
	  		</div><!-- list-item -->
	  		</s:iterator>
		</div>
	</div>
  </body>
</html>