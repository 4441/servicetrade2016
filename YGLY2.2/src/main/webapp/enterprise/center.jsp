<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@taglib prefix="s" uri="/struts-tags" %>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <base href="<%=basePath%>">
	
	<link rel="stylesheet" href="css/enterprise.css">
  </head>
  
  <body>
  	<jsp:include page="/public/head.jsp"></jsp:include>
  		<div class="menu">
  			<div class="menu-head">
  				<div class="picture">
    				<a href="enterprise/picture.jsp"><img src="img/bg.jpg"></a>
				</div>
				<div class="information">
					${user.user}
				</div>
  			</div>
  			<div class="item-list">
  				<div class="item">
  					<a href="enterprise/dynamic.jsp" class="item-href">动态</a>
  				</div>
  				<div class="item">
  					<a href="enterprise/message.jsp" class="item-href">私信</a>
  				</div>
  				<div class="item">
  					<a href="enterprise/projectmanage.jsp" class="item-href">项目管理</a>
  				</div>
  				<div class="item">
  					<a href="enterprise/personaldata.jsp" class="item-href">企业资料</a>
  				</div>
  				<div class="item">
  					<a href="enterprise/accountdata.jsp" class="item-href">修改账户</a>
  				</div>
  			</div>
  			
  			<div class="release-item" style="height:0px;border:0;padding:0;">
  			</div>

  			<div class="item-list">
  				<div class="item">
  					<a href="enterprise/release.jsp" class="item-href">创建新项目</a>
  				</div>
  				<s:if test="#session.user.user=='admin'">
  				<div class="item">
  					<a href="admin/e-reg.jsp" class="item-href">录入企业</a>
  				</div>
  				<div class="item">
  					<a href="admin/news.jsp" class="item-href">发布公告</a>
  				</div>
  				</s:if>
  			</div>
  			
  		</div>
  </body>
</html>