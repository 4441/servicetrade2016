<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <base href="<%=basePath%>">
	
	<link rel="stylesheet" href="css/message.css">
  </head>
  
  <body>
  	<jsp:include page="center.jsp"></jsp:include>
  	<div class="content">
  		<div class="project-list">
  			<div class="list-item">
  				<div class="item-left">
  					<div class="left-up">
  						来自<a href="javascript:void()" class="item-href1">系统</a>的消息
  					</div>
  					<div class="left-down">
  						你好
  					</div>
  				</div>
  				<div class="item-right">
  					<div>
  						4-12
  					</div>
  				</div>
  			</div><!-- list-item -->
  		</div>
  	</div>
  </body>
</html>