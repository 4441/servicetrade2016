<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <base href="<%=basePath%>">
    
	<link rel="stylesheet" href="css/public.css">
	<link rel="stylesheet" href="css/eproject.css">
  </head>
  
  <body>
  	<jsp:include page="../public/head.jsp"></jsp:include>
  	<div class="content">
  		<div class="title">
  			<div class="title-left">
  				<a href="javascript:void();" class="title-href"></a>
  			</div>
  			<div class="title-right">
  				<span class="btn-span1">
  					<button class="title-btn">内容编辑</button>
  				</span>
  				<span class="btn-span1">
  					<a href="enterprise/member.jsp;"><button class="title-btn">成员管理</button></a>
  				</span>
  			</div>
  		</div>
  		<div class="intro">
  			一个Chrome浏览器插件，用于生成当前URL或者选中内容的二维码，同时可以用于解析网页上的二维码内容。(项目简介)
  		</div>
  		<div class="detail">
  			项目详情
  		</div>
  		<div class="commend-head">项目评论</div>
  		<div class="commend">
  		
  			<div class="commend-item">
  				<div class="item-left">T</div>
  				<div class="item-right">
  					<div class="up">
  						<a href="javascript:void();" class="user-href">用户名 </a>时间
  					</div>
  					<div class="down">
  						评论
  					</div>
  				</div>
  			</div><!-- commend-item -->
  			
  			<%
  			for(int i=1;i<10;i++){
  			 %>
  			 <div class="commend-item">
  				<div class="item-left">T</div>
  				<div class="item-right">
  					<div class="up">
  						<a href="javascript:void();" class="user-href">用户名 </a>时间
  					</div>
  					<div class="down">
  						评论
  					</div>
  				</div>
  			</div><!-- commend-item -->
  			<%
  			}
  			 %>
  			 
  			 <div class="commend-item-mine">
  				<span class="span1">T</span>
  				<span class="span2">
  					<input type="text" class="commend-inputbox">
  				</span>
  				<span class="span3">
  					<button class="commend-btn">评论</button>
  				</span>
  			</div><!-- commend-item -->
  			
  		</div>
  		<div class="tail">
  			
  		</div>
  	</div>
  </body>
</html>
