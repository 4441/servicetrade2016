<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <base href="<%=basePath%>">
	
	<link rel="stylesheet" href="css/release.css">
  </head>
  
  <body>
  	<jsp:include page="center.jsp"></jsp:include>
  	<div class="content">
  		<div class="head">
  			<h3>创建项目</h3>
  		</div>
  		<div class="project-form">
  		<form action="Create" method="post">
  			<div class="form-item">
  				<span class="item-left">
  					<label class="item-label">项目名</label>
  				</span>
  				<span class="item-right">
  					<input name="pro.name" class="item-inputbox" type="text"/>
  				</span>
  			</div>
  			<div class="form-item">
  				<span class="item-left">
  					<label class="item-label">项目简介</label>
  				</span>
  				<span class="item-right">
  					<textarea name="pro.intr" class="item-textarea"></textarea>
  				</span>
  			</div>
  			<div class="form-item">
  				<span class="item-left">
  					<label class="item-label">项目详情</label>
  				</span>
  				<span class="item-right">
  					<textarea name="pro.content" class="item-textarea" style="height:90px;"></textarea>
  				</span>
  			</div>
  			<div class="form-item">
  				<span class="item-left">
  					<label class="item-label">项目属性</label>
  				</span>
  				<span class="item-right">
  					<input class="" type="checkbox" name="pro.open" value="1"/>公开
  				</span>
  			</div>
  			<div class="form-item">
  				<span class="item-left">
  				</span>
  				<span class="item-right">
  					<button class="btn">创建</button>
  					<button class="btn">取消</button>
  				</span>
  			</div>
  		</form>
  		</div>
  	</div>
  </body>
</html>