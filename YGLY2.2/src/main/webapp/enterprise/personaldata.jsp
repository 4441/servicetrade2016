<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <base href="<%=basePath%>">
	<link rel="stylesheet" href="css/personaldata.css">
  </head>
  
  <body>
  	<jsp:include page="center.jsp"></jsp:include>
  	<div class="content">
  		<h3>企业信息</h3>
  		
  		<label>企业名称</label><br>
  		<div class="text-box1">
  			<input type="text" name="" placeholder="名称" required="required" class="item-row" value="${user.name}" />
  		</div><br>
  		
  		<label>企业介绍</label><br>
  		<div class="text-box1">
  			<textarea name="" rows="7" class="textarea-box" placeholder="企业介绍">${user.introduction}</textarea>
  		</div><br>
  		
  		<h3>更多信息</h3>
  		
  		<label>所在省份</label>
  		<label class="label-distance">所在城市</label><br>
  		<div class="text-box2">
  			<input type="text" name="" placeholder="省份" required="required" class="item-row2"  />&nbsp&nbsp&nbsp
  			<input type="text" name="" placeholder="城市" required="required" class="item-row2"  />
  		</div><br>
  		
  		<label>所在地区</label>
  		<label class="label-distance">联系方式</label><br>
  		<div class="text-box2">
  			<input type="text" name="" placeholder="地区" required="required" class="item-row2"  />&nbsp&nbsp&nbsp
  			<input type="text" name="" placeholder="联系方式" required="required" class="item-row2"  />
  		</div><br>
  		<div class="item-row">
  				<button class="login-btn">更新</button>
  		</div>
  	</div>
  </body>
</html>